//
// Created by Max Leingartner on 13.12.18.
//

#ifndef WORDCOUNTER_HELPER_H
#define WORDCOUNTER_HELPER_H

#include <exception>

class FileException : public std::exception {
    const char* msg;
    const std::string filename;

public:
    FileException(const char* msg, const std::string &filename) : msg(msg), filename (filename)
    {    }

    const char * what () const throw ()
    {
        return msg;
    }

    const std::string getFilename() const { return filename; }
};

#endif //WORDCOUNTER_HELPER_H
