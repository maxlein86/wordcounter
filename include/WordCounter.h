//
// Created by Max Leingartner on 12.12.18.
//

#ifndef WORDCOUNTER_H
#define WORDCOUNTER_H

#include <functional>
#include <future>
#include <map>
#include <vector>
#include <list>

// Template for an async task call
template<typename F, typename... Ts>
inline
auto
asyncTask(F &&f, Ts &&... params)
{
    return std::async(std::launch::async,
                      std::forward<F>(f),
                      std::forward<Ts>(params)...);
}

using WordCountMap = std::map<std::string, long>;
using WordCountPair = std::pair<std::string, long>;
using Task = std::future<WordCountMap>;
using TaskContainer = std::vector<Task> ;
using TaskIterator = TaskContainer::iterator;


class WordCounter {
public:
    void addFileToProcess(const std::string &filename);

    void processFiles();

    WordCountMap countWordsInFile(const std::string &filename);

    void clearFiles();

    void printResult() const;

    const WordCountMap &getWordCountMap() const;

private:
    std::mutex mapMutex;

    std::vector<std::string> filesToProcess;

    std::list<std::string> failedFileList;

    TaskContainer tasks;

    WordCountMap wordCountMap;

    void mergeResults();

    void lookForFinishedTask();

    void processTaskResult(TaskIterator &task);

    void mergeMapToTargetMap(const WordCountMap &map);

    void printFailedFilesList() const;

    };


#endif //WORDCOUNTER_H
