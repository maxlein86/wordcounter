//
// Created by Max Leingartner on 13.12.18.
//

#ifndef WORDCOUNTER_PRETTYPRINTER_H
#define WORDCOUNTER_PRETTYPRINTER_H

#include <iostream>
#include <iomanip>

using namespace std;

namespace printer {
    constexpr auto separator = ' ';
    constexpr auto width = 15;

    template<typename Word, typename Count>
    inline void printWordCount(const Word &word, const Count &count) {
        cout << left << setw(width) << setfill(separator) << word << right << setw(width) << setfill(separator) << count << endl;
    }
}
#endif //WORDCOUNTER_PRETTYPRINTER_H
