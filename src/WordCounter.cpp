//
// Created by Max Leingartner on 12.12.18.
//

#include <string>
#include <fstream>
#include <iostream>
#include "WordCounter.h"
#include "PrettyPrinter.h"
#include "FileException.h"

void WordCounter::addFileToProcess(const std::string &filename) {
    filesToProcess.emplace_back(filename);
}

void WordCounter::processFiles() {
    tasks.clear();
    wordCountMap.clear();

    for (const auto &filename : filesToProcess) {
        tasks.push_back(asyncTask(&WordCounter::countWordsInFile, this, filename));
    }

    mergeResults();
}

WordCountMap WordCounter::countWordsInFile(const std::string &filename) {
    WordCountMap wordCountMap;
    std::ifstream file(filename);

    if (!file.is_open()) {
        throw FileException("Could not open file!", filename);
    }
    std::string word;

    while (file >> word) // check Space,LF, CR, ...
    {
        auto element = wordCountMap.find(word);
        auto wordMissingInMap = element == wordCountMap.end();

        if (wordMissingInMap) {
            wordCountMap.emplace(WordCountPair(word, 1));
        } else {
            auto &count = element->second;
            count++;
        }
    }

    file.close();
    return std::move(wordCountMap);
}

void WordCounter::mergeResults() {
    while (!tasks.empty()) {
        lookForFinishedTask();
    }
}

void WordCounter::lookForFinishedTask() {
    auto task = tasks.begin();
    while (task != tasks.end()) {
        if (task->wait_for(std::chrono::milliseconds(100)) == std::future_status::timeout) {
            ++task;
        } else {
            processTaskResult(task);
        }
    }
}

void WordCounter::processTaskResult(TaskIterator &task) {
    try {
        auto map = task->get();
        mergeMapToTargetMap(map);
    }
    catch (const FileException &e) {
        failedFileList.push_back(e.getFilename());
        std::cout << "Error: " << e.what() << " File: " << e.getFilename() << '\n';
    }
    catch (...) {
        std::cerr << "Unknown file error" << '\n';
    }
    task = tasks.erase(task);
}

void WordCounter::mergeMapToTargetMap(const WordCountMap &map) {
    std::lock_guard<std::mutex> lock(mapMutex);

    for (auto it = map.begin(); it != map.end(); ++it) {
        if (wordCountMap[it->first]) {
            wordCountMap[it->first] += it->second;
        } else {
            wordCountMap[it->first] = it->second;
        }
    }
}

void WordCounter::printResult() const {
    printer::printWordCount("Wort", "Anzahl");

    for (auto element : wordCountMap) {
        printer::printWordCount(element.first, element.second);
    }

    if (failedFileList.size() != 0) {
        printFailedFilesList();
    }
}

void WordCounter::printFailedFilesList() const {
    std::cout << "\nFailed to process these files:\n";
    for (const auto &file : failedFileList) {
        std::cout << file << std::endl;
    }
}

const WordCountMap &WordCounter::getWordCountMap() const {
    return wordCountMap;
}

void WordCounter::clearFiles() {
    filesToProcess.clear();
}
