#include <iostream>
#include <fstream>
#include <map>
#include <future>
#include <chrono>
#include <vector>
#include <stdexcept>
#include <WordCounter.h>


int main(int argc, char **argv) {
    std::cout << "You have entered " << argc << " arguments:" << "\n";

    WordCounter wordCounter;

    for (unsigned fileCounter = 1; fileCounter < argc; ++fileCounter) {
        wordCounter.addFileToProcess(argv[fileCounter]);
    }

    wordCounter.processFiles();

    wordCounter.printResult();

    return EXIT_SUCCESS;
}
