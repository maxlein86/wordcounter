#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "WordCounter.h"
#include <chrono>

// Not real unit tests, but nice for testing ;)

TEST_CASE( "CountWordsInFile" )
{
    WordCounter wordCounter;
    wordCounter.addFileToProcess("test.txt");

    wordCounter.processFiles();
    wordCounter.printResult();

    auto map = wordCounter.getWordCountMap();
    REQUIRE( map["filii"] == 6 );
}

TEST_CASE( "MissingFile" )
{
    WordCounter wordCounter;
    wordCounter.addFileToProcess("missing.txt");

    wordCounter.processFiles();
    wordCounter.printResult();

    auto map = wordCounter.getWordCountMap();
    REQUIRE( map.size() == 0 );
}

TEST_CASE( "ParallelVsSerial" )
{
    auto numFiles = 100;

    auto startTime = std::chrono::high_resolution_clock::now();
    WordCounter wordCounter;
    for (int i = 0; i < numFiles; ++i) {
        wordCounter.addFileToProcess("test.txt");
    }

    wordCounter.processFiles();
    auto durationParallel = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - startTime).count();

    wordCounter.printResult();
    auto map = wordCounter.getWordCountMap();
    REQUIRE( map["filii"] == 6 * numFiles );

    //serial
    startTime = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < numFiles; ++i) {
        WordCounter wordCounter;
        wordCounter.addFileToProcess("test.txt");
        wordCounter.processFiles();
    }

    auto durationSerial = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - startTime).count();
    printf("parallel: %lld [ms], serial: %lld [ms]", durationParallel, durationSerial);

    REQUIRE( durationSerial > durationParallel );
}