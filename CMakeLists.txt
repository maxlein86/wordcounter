cmake_minimum_required(VERSION 3.10)
project(WordCounter)

set(CMAKE_CXX_STANDARD 14)

find_package (Threads)

include_directories(include)

add_executable(word_counter src/main.cpp src/WordCounter.cpp)
target_link_libraries (word_counter ${CMAKE_THREAD_LIBS_INIT})
install(TARGETS word_counter DESTINATION ${PROJECT_SOURCE_DIR}/bin)

add_executable(some_tests tests/main_test.cpp src/WordCounter.cpp)
target_link_libraries (some_tests ${CMAKE_THREAD_LIBS_INIT})
install(TARGETS some_tests DESTINATION ${PROJECT_SOURCE_DIR}/bin/tests)
install(FILES resources/test.txt DESTINATION ${PROJECT_SOURCE_DIR}/bin/tests)

add_custom_command(
        TARGET some_tests POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
        ${CMAKE_SOURCE_DIR}/resources/test.txt
        ${CMAKE_CURRENT_BINARY_DIR}/test.txt)
